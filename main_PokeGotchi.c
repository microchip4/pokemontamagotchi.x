/*
 * File:   main_PokeGotchi.c
 * Author: Brandy
 *
 * Created on October 24, 2016, 4:10 PM
 */

#include <xc.h>
#include "Alteri.h"
#include "font_pattern.h"
#include "Nokia5110.h"
#include "Drifblim.h"
#include "Drifloon.h"
#include "Egg.h"
#include "FoodPIX.h"

void Evolution1(void);
void Evolution2(void);
void GiveFood(char FoodConst);
void Function_2(void);

char ACC = 0;

void main(void) {
    TRISB=0;
    Nokia5110_Init();
    Nokia5110_Clear();
    delay_ms(10);
    while(1){
        Nokia5110_Bitmap(Egg,0,0);
        Nokia5110_WriteString("Egg",30,5);
        //GiveFood(ACC);
        ACC++;
        delay_ms(1000);
        Evolution1();
        //GiveFood(ACC);
        ACC++;
        Evolution2();
        //GiveFood(ACC);
        ACC++;
    }
    return;
}

void Evolution1(void){
    Nokia5110_Clear();
    int i;
    for (i = 0; i < 10; i++) {
        Nokia5110_Bitmap(Egg,0,0);
        delay_ms(100);
        Nokia5110_Clear();
        Nokia5110_Bitmap(Drifloon,0,0);
        delay_ms(100);
        Nokia5110_Clear();
    }
    Nokia5110_Bitmap(Drifloon,0,0);
    Nokia5110_WriteString("Drifloon",10,5);
    delay_ms(1000);
}

void Evolution2(void){
    Nokia5110_Clear();
    int i;
    for (i = 0; i < 10; i++) {
        Nokia5110_Bitmap(Drifloon,0,0);
        delay_ms(100);
        Nokia5110_Clear();
        Nokia5110_Bitmap(Drifblim,0,0);
        delay_ms(100);
        Nokia5110_Clear();
    }
    Nokia5110_Bitmap(Drifblim,0,0);
    Nokia5110_WriteString("Drifblim",10,5);
    delay_ms(1000);
}

void GiveFood(char FoodConst){
    int i = FoodConst*10;
    Nokia5110_GotoXY(10,2);
    for (i = 0; i < i+10; i++) {
        Nokia5110_WriteByte(FoodPIX[i]);
    }
}